package cest;

import java.util.ArrayList;

import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.Pessoa;

public class ListasArrays {

	public static void main(String[] args) {
		ArrayList lista = new ArrayList();
		Animal passaro = new Animal();
		passaro.setNome("Hades");
		
		Animal cachorro = new Animal();
		cachorro.setNome("Cerberos");
		
		Animal elefante = new Animal();
		elefante.setNome("Floki");
		
		Pessoa pessoa = new Pessoa();
		pessoa.setNome("Lucifer");
		lista.add(passaro);
		lista.add(cachorro);
		lista.add(elefante);
		lista.add(pessoa);
				
		//for (Animal ani:lista)System.out.println("Nome: + ani.getNome{
	
		//Criando laço for
		for (int i= 0;  i < lista.size(); i++ ) {
		System.out.println("O nome do Animal:" + ((Animal)lista.get(i)).getNome());
		
		}
		
		
	
		
		
		
		
	}

}
