package cest.mypet;

import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.Pessoa;
import cest.mypet.cadastro.TipoAnimal;
import cest.mypet.cadastro.loc.Cidade;
import cest.mypet.cadastro.loc.UF;


public class Principal {
	
	public static void main(String[] args) {
	TipoAnimal raca = new TipoAnimal();
	raca.setDescricao (": Poodle");
	raca.setCod (123);
	Animal cachorro = new Animal();
	cachorro.setNome (": Lulu");
	cachorro.setIdade(2);
	cachorro.setTipoanimal(raca);
	Pessoa pessoa1 = new Pessoa();
	pessoa1.setNome (" Diego");
	pessoa1.setIdade( 32 );
	UF uf1 = new UF();
	uf1.setCod (" RJ");
	uf1.setDescricao (" Não faço ideia");
	Cidade cidade1 = new Cidade();
	cidade1.setNome (" Duque de Caxias");
	cidade1.setUf(uf1);
	
	
	System.out.println("Nome do cachorro: " + cachorro.getNome());
	System.out.println("Idade do cachorro: " + cachorro.getIdade());
	System.out.println("Descrição do animal: " + cachorro.getTipoAnimal().getDescricao());
	System.out.println("Codigo do animal: " + cachorro.getTipoAnimal().getCod());
	System.out.println("Nome da pessoa: " + pessoa1.getNome());
	System.out.println("Idade da pessoa: " + pessoa1.getIdade());
	System.out.println("Nome da cidade" + cidade1.getNome());
	System.out.println("Codigo UF: " + cidade1.getUf().getCod());
	System.out.println("Descrição da UF: " + cidade1.getUf().getDescricao());
	
	}
}
