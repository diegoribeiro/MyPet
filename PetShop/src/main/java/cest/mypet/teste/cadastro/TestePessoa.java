package cest.mypet.teste.cadastro;

import cest.mypet.cadastro.Pessoa;

public class TestePessoa {

	public static void main(String[] args) {
		Pessoa p = new Pessoa();
		p.setNome("Jose dos Anzois");
		
		Pessoa p2 = new Pessoa();
		p2.setNome("Jose das Limeiras");
		
		System.out.println("P1: " + p.getNome());
		System.out.println("P2: " + p2.getNome());
		

	}

}
