package cest.mypet.cadastro.loc;

	public class UF {
		private String cod;
		private String descricao;
		
		public void setCod(String cod) {
			this.cod = cod;
		}
		public String getCod() {
			return this.cod;
		}
		
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public String getDescricao() {
			return this.descricao;
		}
}
	
	
