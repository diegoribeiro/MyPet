package cest.mypet.cadastro.loc;

	public class Cidade {
	private UF uf;
	private String nome;
	
	
	public void setNome(String nome) {
	this.nome = nome;
	}
	
	public String getNome() {
		 return this.nome;
	}
	
	public void setUf(UF uf) {
		this.uf = uf;
	}
	
	public UF getUf() {
		return this.uf;
	}

}
