package cest.mypet.cadastro;

import cest.mypet.cadastro.loc.Cidade;

public class Pessoa {
	private String nome;
	private int idade;
	private Cidade cidade;
	
	public void setNome(String nome) {
		this.nome = nome;	
	}
	public String getNome() {
		return this.nome;	
	}
	
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public int getIdade() {
		return this.idade;
	}
	public Cidade getCidade() {
		return cidade;
		}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
}
