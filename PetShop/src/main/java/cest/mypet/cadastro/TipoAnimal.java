package cest.mypet.cadastro;

	public class TipoAnimal {
	private int cod;
	private String descricao;
	
	public void setCod(int cod) {
		this.cod = cod;
	}
	public int getCod() {
		return this.cod;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return this.descricao;
	}

}
