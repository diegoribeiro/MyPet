package cest.mypet.cadastro;

public class Animal {
	private String nome;
	private int idade;
	private TipoAnimal tipo;
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
	return this.nome;
	}
	
	public void setIdade (int idade) {
		this.idade = idade; 
	}
	
	public int getIdade() {
		return this.idade;
	}
	
	public void setTipoanimal(TipoAnimal tipo) {
		this.tipo = tipo;
	}
	public TipoAnimal getTipoAnimal(){
	return this.tipo;
	}
	
}